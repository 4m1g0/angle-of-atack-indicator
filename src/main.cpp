#include "Arduino.h"
#include "Wire.h"
#include <QMC5883L.h>
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include "config/config.h"
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>

QMC5883L compass;
WebSocketsServer webSocket = WebSocketsServer(81);
ESP8266WebServer server(80);
const char* host = "aoa";

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length);
bool handleFileRead(String path);

void setup()
{
	Wire.begin();
  Serial.begin(9600);
  SPIFFS.begin();
  MDNS.begin(host);

  // setup wifi connection
  WiFi.softAP(WIFI_SSID, WIFI_PASSWORD);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);

	compass.init();
	compass.setSamplingRate(50);

  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  server.onNotFound([]() {
    if (!handleFileRead(server.uri())) {
      server.send(404, "text/plain", "FileNotFound");
    }
  });
  server.begin();
}

void loop()
{
	webSocket.loop();
  server.handleClient();
  MDNS.update();

  int16_t x, y, z, t;
  if (!compass.ready())
    Serial.println("NO!");
  
  compass.readRaw(&x, &y, &z, &t);

  Serial.print("x: ");
  Serial.print(x);
  Serial.print(", y: ");
  Serial.print(y);
  Serial.print(", z: ");
  Serial.print(z);

  float angle = atan2(y, x);

  Serial.print(",  dir: ");
  Serial.println(angle);

  static unsigned long lastSend = 0;

  static double sumAngle = 0;
  static unsigned int samples = 0;
  sumAngle += ((double)angle);
  samples++;

  if (millis() - lastSend > 200){
    lastSend = millis();
    String angleTxt = String(((double)sumAngle)/samples);
    webSocket.broadcastTXT(angleTxt);
    samples = 0;
    sumAngle = 0;
  }
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED:
            {
                IPAddress ip = webSocket.remoteIP(num);
                Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
				
				// send message to client
				webSocket.sendTXT(num, "Connected");
            }
            break;
        case WStype_TEXT:
            Serial.printf("[%u] get Text: %s\n", num, payload);

            // send message to client
            // webSocket.sendTXT(num, "message here");

            // send data to all connected clients
            // webSocket.broadcastTXT("message here");
            break;
        case WStype_BIN:
            Serial.printf("[%u] get binary length: %u\n", num, length);
            hexdump(payload, length);

            // send message to client
            // webSocket.sendBIN(num, payload, length);
            break;
    }

}

String getContentType(String filename) {
  if (server.hasArg("download")) {
    return "application/octet-stream";
  } else if (filename.endsWith(".htm")) {
    return "text/html";
  } else if (filename.endsWith(".html")) {
    return "text/html";
  } else if (filename.endsWith(".css")) {
    return "text/css";
  } else if (filename.endsWith(".js")) {
    return "application/javascript";
  } else if (filename.endsWith(".png")) {
    return "image/png";
  } else if (filename.endsWith(".gif")) {
    return "image/gif";
  } else if (filename.endsWith(".jpg")) {
    return "image/jpeg";
  } else if (filename.endsWith(".ico")) {
    return "image/x-icon";
  } else if (filename.endsWith(".xml")) {
    return "text/xml";
  } else if (filename.endsWith(".pdf")) {
    return "application/x-pdf";
  } else if (filename.endsWith(".zip")) {
    return "application/x-zip";
  } else if (filename.endsWith(".gz")) {
    return "application/x-gzip";
  }
  return "text/plain";
}

bool handleFileRead(String path) {
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) {
    path += "index.htm";
  }
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {
    if (SPIFFS.exists(pathWithGz)) {
      path += ".gz";
    }
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, contentType);
    file.close();
    return true;
  }
  return false;
}

